﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Connection.Controllers
{
    public class HomeController : Controller
    {
        private static String connectionString = ConfigurationManager.ConnectionStrings["cs"].ConnectionString;
        private string SELECT = "select name from sys.databases";

        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.databases = GetDatabases();
            return View();
        }
        [HttpPost]
        public ActionResult ConnectToDatabases()
        {

            OpenCloseAll(Request.Form.AllKeys);
            ViewBag.databases = GetDatabases();
            return View("Index");
        }

        private void OpenCloseAll(string[] allKeys)
        {

            IList<String> log = Session["log"] == null ? new List<String>() : Session["log"] as IList<String>;
            IList<SqlConnection> sqlConnections = new List<SqlConnection>();
            log.Add($"====CONNECTIONS OPENING====");
            foreach (var item in allKeys)
            {
                try
                {
                    SqlConnection sqlConnection = new SqlConnection(connectionString + $";Database={item}");

                    sqlConnection.Open();
                  
                    log.Add($"Database connection open to {item}");
                    sqlConnections.Add(sqlConnection);
                }
                catch (Exception ex)
                {
                    log.Add($"Database connection failed for {item}. Message: {ex.Message}");
                }

            }
            log.Add($"----CONNECTIONS CLOSING----");
            foreach (var item in sqlConnections)
            {
                try
                {
                    item.Close();
                    log.Add($"Database connection closed to {item.Database}");
                }
                catch (Exception ex)
                {
                    log.Add($"Database connection close failed for {item}. Message: {ex.Message}");
                }

            }
            Session["log"] = log;

        }

        private IList<String> GetDatabases()
        {
            IList<String> availabeDatabases = new List<String>();
            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();
                using (SqlCommand getDatabasesCommand = new SqlCommand(SELECT, sqlConnection))
                using (SqlDataReader sqlDataReader = getDatabasesCommand.ExecuteReader())
                    while (sqlDataReader.Read())
                        availabeDatabases.Add(sqlDataReader.GetString(0));

            }
            return availabeDatabases;
        }
    }
}