use master
go
drop database Diablo
go 
create database Diablo
go
use Diablo
go
create table TipNamjernice(
IDTipNamjernice int constraint PK_IDTipNamjernice primary key identity,
Naziv nvarchar(50) not null
)
go
create table Namjernica
(
IDNamjernica int constraint PK_IDNamjernice primary key identity,
Naziv nvarchar(50) not null,
TipNamjerniceID int constraint FK_TipnamjerniceID foreign key references TipNamjernice(IDTipNamjernice) not null,
Deleted bit default 0
)
go
create table MjernaJedinica
(
IDMjernaJedinica int constraint PK_MjernaJedinica primary key identity,
Naziv nvarchar(50),
Deleted bit default 0
)
go
create table MjernaJedinicaZaNamjernicu
(
IDMjernaJedinicaZaNamjernicu int constraint PK_MjernaJedinicaZaNamjernicu primary key identity,
MjernaJedinicaID int constraint FK_MjernaJedinicaID foreign key references MjernaJedinica(IDMjernaJedinica),
NamjernicaID int constraint FK_NamjernicaID foreign key references Namjernica(IDNamjernica),
KalorijskaVrijednost int
)
go
create table RazinaAktivnosti
(
IDRazinaAktivnosti int constraint PK_IDRazinaAktivnosti primary key identity,
Naziv nvarchar(50)
)
go
create table TipDijabetesa
(
IDTipDijabetesa int constraint PK_IDTipDijabetesa primary key identity,
Naziv nvarchar(50)
)
go
create table Spol
(
IDSpol int constraint PK_IDSpol primary key identity,
Naziv nvarchar(50)
)
go
create table Osoba
(
IDOsoba int constraint PK_IDOsoba primary key identity,
Ime nvarchar(60),
Prezime nvarchar(60),
KorisnickoIme nvarchar(60),
Email nvarchar(60),
Lozinka nvarchar(MAX),
isAdmin bit,
)
go
create table Korisnik
(
IDKorisnik int constraint PK_Korisnik primary key identity,
OsobaID int constraint FK_OsobaID foreign key references Osoba(IDOsoba),
DatumRodenja date,
SpolID int constraint FK_SpolID foreign key references Spol(IDSpol),
TipDijabetesaID int constraint FK_TipDijabetesaID foreign key references TipDijabetesa(IDTipDijabetesa),
RazinaFizickeAktivnosti int constraint FK_RazineFizickeAktivnosti foreign key references RazinaAktivnosti(IDRazinaAktivnosti),
Tezina float,
Visina float
)
go
create table Obrok
(
IDObrok int constraint PK_IDObrok primary key identity,
Naziv nvarchar(50),
PostotakUgljikohidrata int,
CHECK(PostotakUgljikohidrata > 0),
CHECK(PostotakUgljikohidrata < 101),

PostotakBjelancevina int,
CHECK(PostotakBjelancevina > 0),
CHECK(PostotakBjelancevina < 101),

PostotakMasti int,
CHECK(PostotakMasti > 0),
CHECK(PostotakMasti < 101),
Deleted bit default 0
)
go
Create table DnevniSetObroka
(
IDDnevniSetObroka int constraint PK_IDDnevniSetObroka primary key identity,
Naziv nvarchar(50),
BrojObrokaUDanu int
Check (BrojObrokaUDanu <= 7),
Check (BrojObrokaUDanu >0),
Deleted bit default 0,
Selected bit default 0
)
go
Create table ObrokUKombinaciji
(
IDObrokUKombinaciji int constraint PK_IDObrokUKombinaciji primary key identity,
DnevniSetObrokaID int constraint FK_DnevniSetObroka foreign key references DnevniSetObroka(IDDnevniSetObroka),
ObrokID int constraint FK_Obrok foreign key references Obrok(IDObrok),
PostotakHranjiveVrijednost int,
CHECK(PostotakHranjiveVrijednost > 0),
CHECK(PostotakHranjiveVrijednost < 101)
)
go
create table Jelovnik
(
IDJelovnik int constraint PK_IDJelovnik primary key identity,
KorisnikID int constraint PK_KorisnikJelovnikID foreign key references Korisnik(IDKorisnik),
DatumStvaranja datetime,
HranjivaVrijednostPotrebna int default 2000
)
go
create table StavkaJelovnika
(
IDStavkaJelovnika int constraint PK_IDStavkaJelovnika primary key identity,
JelovnikID int constraint FK_Jelovnik foreign key references Jelovnik(IDJelovnik),
ObrokUKombinacijiID int constraint FK_ObrokUKombinaciji foreign key references ObrokUKombinaciji(IDObrokUKombinaciji)
)
go
create table StavkaStavkeJelovnika
(
IDStavkaStavkeJelovnika int constraint PK_StavkaStavkeJelovnika primary key identity,
StavkaJelovnikaID int constraint FK_StavkaJelovnika foreign key references StavkaJelovnika(IDStavkaJelovnika),
NamjernicaID int constraint FK_Namjernica foreign key references Namjernica(IDNamjernica)
)
go
--Population
insert into RazinaAktivnosti(Naziv)
values('Visoka'),('Srednja'),('Niska')
go
insert into Spol(Naziv)
values('Zensko'),('Musko')
go
insert into Osoba(Ime,Prezime,KorisnickoIme, Email, Lozinka, isAdmin)
values('Lorem','Ipsum','sa','Lorem@Ipsum.net','6F3D860525DA2B9A169D94603731E792BE079D8F1030928E03014EECFBA447E2',1), ('Random','Person','boringPerson','Lorem@Ipsum.net','6F3D860525DA2B9A169D94603731E792BE079D8F1030928E03014EECFBA447E2',0), ('Lazy','Person','lazyPerson','Lorem@Ipsum.net','6F3D860525DA2B9A169D94603731E792BE079D8F1030928E03014EECFBA447E2',0),('Temp','Placeholder','temp','Lorem@Ipsum.net','6F3D860525DA2B9A169D94603731E792BE079D8F1030928E03014EECFBA447E2',0) 
go
insert into TipDijabetesa(Naziv)
values('Tip I'),('Tip II')
go
insert into Korisnik(OsobaID,DatumRodenja,SpolID,TipDijabetesaID,RazinaFizickeAktivnosti,Tezina,Visina)
values(2,'1971-07-28 00:00:00.000',2,1,1,85,1.88), (3,'1990-02-14 00:00:00.000',2,1,3,80,1.7),(4,'1986-03-28 00:00:00.000',1,1,3,80,1.7)
go
go
insert into TipNamjernice(Naziv)
values('Masti'),('Ugljikohidrati'),('Bjelančevine')
go
insert into MjernaJedinica(Naziv)
values('100 g'),('Salica'),('Komad')
go
insert into Namjernica(Naziv,TipNamjerniceID)
values('Spek', 3), ('Cokolada',2), ('Mast',1), ('Jabuka',2),('Pohano meso pilece', 3), ('Pomfri iz McDonaldsa',1)
go
insert into Jelovnik(KorisnikID,DatumStvaranja)
values(1,'2019-07-17 00:00:00.000'),(1,'2019-07-10 00:00:00.000'),(1,'2019-07-13 00:00:00.000'),(3,'2019-07-12 00:00:00.000'),(3,'2019-07-13 00:00:00.000')
go
insert into DnevniSetObroka(Naziv,BrojObrokaUDanu, Selected)
values('Hello world kombinacija',2,1),('Advanced kombinacija',3,1)
go
insert into Obrok(Naziv,PostotakBjelancevina,PostotakMasti,PostotakUgljikohidrata)
values('Dorucak', 40,10,50),('Rucak', 33,33,33),('Vecera', 10,10,80)
go
insert into ObrokUKombinaciji(DnevniSetObrokaID,ObrokID,PostotakHranjiveVrijednost)
values(1,1,35),(1,2,65),(2,1,20),(2,2,70),(2,3,10)
go
insert into MjernaJedinicaZaNamjernicu(MjernaJedinicaID,NamjernicaID,KalorijskaVrijednost)
values(1,1,40),(3,1,65),(1,2,25),(1,3,50),(2,3,100),(1,4,77),(3,4,14),(1,5,43),(1,6,60),(2,6,38)
go
insert into StavkaJelovnika(JelovnikID,ObrokUKombinacijiID)
values(1,1),(1,2),(2,1),(2,2),(3,3),(3,4),(3,5),(4,1),(4,2),(5,1),(5,2)
go
insert into StavkaStavkeJelovnika(StavkaJelovnikaID,NamjernicaID)
values(1,1),(1,2),(1,3),(2,4),(2,5),(2,6),(3,1),(3,2),(3,3),(4,4),(4,5),(4,6),(5,1),(5,2),(5,3),(6,4),(6,5),(6,6),(7,5),(7,2),(7,3),(8,1),(8,2),(8,3),(9,4),(9,5),(9,6),(10,1),(10,2),(10,3),(11,4),(11,5),(11,6)
go
Create proc P_DohvatiSveKorisnike
as
select o.Ime, o.Prezime, o.Email, o.KorisnickoIme, k.DatumRodenja, s.Naziv as Spol, d.Naziv as Dijabetes, a.Naziv as Aktivnost, k.Tezina, k.Visina from Osoba as o
inner join Korisnik as k
on k.OsobaID = o.IDOsoba
inner join Spol as s
on s.IDSpol = k.SpolID
inner join TipDijabetesa as d
on d.IDTipDijabetesa = k.TipDijabetesaID
inner join RazinaAktivnosti as a
on a.IDRazinaAktivnosti = k.RazinaFizickeAktivnosti
go
--Dohvati Admina
Create proc P_DohvatiAdminaLogin
@KorisnickoIme nvarchar(50),
@Lozinka nvarchar(MAX)
as
select Ime, Prezime from Osoba where KorisnickoIme = @KorisnickoIme and Lozinka = @Lozinka and isAdmin = 1

go
--Pretrazi namirnice
create proc P_PretragaNamjernica
@mjernajedinica int
as
if @mjernajedinica = 0
begin
select n.IDNamjernica,n.Naziv, tn.Naziv as NazivTipa from Namjernica as n
inner join TipNamjernice as tn
on tn.IDTipNamjernice = n.TipNamjerniceID
end
else if @mjernajedinica > 0
begin
select n.IDNamjernica,n.Naziv, tn.Naziv as NazivTipa from Namjernica as n
inner join TipNamjernice as tn
on tn.IDTipNamjernice = n.TipNamjerniceID
where tn.IDTipNamjernice = @mjernajedinica
end
else
begin
select n.IDNamjernica,n.Naziv, tn.Naziv as NazivTipa, ssj.IDStavkaStavkeJelovnika from Namjernica as n
inner join TipNamjernice as tn
on tn.IDTipNamjernice = n.TipNamjerniceID
inner join StavkaStavkeJelovnika as ssj
on ssj.NamjernicaID = IDNamjernica
where StavkaJelovnikaID = (@mjernajedinica * -1)
end
go
--Pretraga mjernih jedinica
create proc P_PretragaMjernihJedinica
@id int
as
if @id = 0
begin
select m.IDMjernaJedinica,m.Naziv from MjernaJedinica as m
where Deleted = 0
end
else
begin
select distinct m.IDMjernaJedinica,m.Naziv, mn.KalorijskaVrijednost from MjernaJedinica as m
inner join MjernaJedinicaZaNamjernicu as mn
on mn.MjernaJedinicaID = m.IDMjernaJedinica
where Deleted = 0 and mn.NamjernicaID = @id
end
go
create proc P_GetPropertyNames
@procName nvarchar(100)
as
SELECT PARAMETER_NAME FROM INFORMATION_SCHEMA.PARAMETERS WHERE SPECIFIC_NAME = @procName
go
create proc P_TipNamirnica
as
select * from TipNamjernice
go
create type Namirnica as table
(
ID int,
Naziv nvarchar(100),
Tip nvarchar(100)
)
go
create type MjernaJedinicaNamjernica as table 
(
ID int,
Naziv nvarchar(100),
Kalorijska int
)
go
Create proc P_UpdateNamirnica
@XML xml
as
declare @tablNamirnica Namirnica
insert into @tablNamirnica
select tblKartica.stupacKartica.value('@IDNamjernica','int') as ID, tblKartica.stupacKartica.value('@Naziv','nvarchar(100)') as Naziv, tblKartica.stupacKartica.value('@NazivTipa','nvarchar(100)') as Tip
   from @XML.nodes('/Namirnica') as tblKartica(stupacKartica)

if (select ID from @tablNamirnica) = 0
begin
insert into Namjernica(Naziv,TipNamjerniceID,Deleted)
values((select Naziv from @tablNamirnica),(Select IDTipNamjernice from TipNamjernice where Naziv = (select Tip from @tablNamirnica)), 0)

update @tablNamirnica
set ID = SCOPE_IDENTITY()
end
else
begin
update Namjernica
set Naziv = (Select Naziv from @tablNamirnica), TipNamjerniceID = (Select IDTipNamjernice from TipNamjernice where Naziv = (select Tip from @tablNamirnica))
where IDNamjernica = (Select ID from @tablNamirnica)
end

DECLARE @cnt INT = 1


WHILE @cnt < (select count(*) from @XML.nodes('/Namirnica/MjerneJediniceList') as tblGrad(stupacGrad)) + 1
BEGIN
	declare @tablMjerna MjernaJedinicaNamjernica
	delete @tablMjerna
	insert into @tablMjerna(ID,Naziv, Kalorijska)
      select tblBolesti.stupacBolest.value('@IDMjernaJedinica','int') as ID,tblBolesti.stupacBolest.value('@Naziv','nvarchar(MAX)') as Naziv, tblBolesti.stupacBolest.value('@KalorijskaVrijednost','int') as Kalorijska
   from @XML.nodes('/Namirnica/MjerneJediniceList[sql:variable("@cnt")]') as tblBolesti(stupacBolest) 

   if(select count(*) from MjernaJedinicaZaNamjernicu where NamjernicaID = (Select ID from @tablNamirnica) and  MjernaJedinicaID = (select IDMjernaJedinica from MjernaJedinica where Naziv = (select Naziv from @tablMjerna))) > 0
   begin
   update MjernaJedinicaZaNamjernicu
   set KalorijskaVrijednost = (select Kalorijska from @tablMjerna)
   where NamjernicaID = (Select ID from @tablNamirnica) and  MjernaJedinicaID = (select IDMjernaJedinica from MjernaJedinica where Naziv = (select Naziv from @tablMjerna))
   end
   else
   begin
   insert into MjernaJedinicaZaNamjernicu(NamjernicaID,MjernaJedinicaID,KalorijskaVrijednost)
   values((select ID from @tablNamirnica),((select IDMjernaJedinica from MjernaJedinica where Naziv = (select Naziv from @tablMjerna))),(Select Kalorijska from @tablMjerna))
   end
   
   SET @cnt = @cnt + 1;
END;


go
Create proc P_UpdateMjerneJedince
@XML xml
as

DECLARE @cnt INT = 1


WHILE @cnt < (select count(*) from @XML.nodes('/ArrayOfMjernaJedinica/MjernaJedinica') as tblGrad(stupacGrad)) + 1
BEGIN
	declare @tablMjerna MjernaJedinicaNamjernica
	delete @tablMjerna
	insert into @tablMjerna(ID,Naziv, Kalorijska)
      select tblBolesti.stupacBolest.value('@IDMjernaJedinica','int') as ID,tblBolesti.stupacBolest.value('@Naziv','nvarchar(MAX)') as Naziv, tblBolesti.stupacBolest.value('@KalorijskaVrijednost','int') as Kalorijska
   from @XML.nodes('/ArrayOfMjernaJedinica/MjernaJedinica[sql:variable("@cnt")]') as tblBolesti(stupacBolest) 

   if(select ID from @tablMjerna) > 0
   begin
   update MjernaJedinica
   set Naziv = (select Naziv from @tablMjerna)
   where IDMjernaJedinica = (Select ID from @tablMjerna)
   end
   else
   begin
   insert into MjernaJedinica(Naziv,Deleted)
   values((select Naziv from @tablMjerna),0)
   end
   
   SET @cnt = @cnt + 1;
END;

go
create proc P_Obroci
@IDkombinacije int
as
if @IDkombinacije = 0
begin
select * from Obrok
where Deleted = 0;
end
else
begin
select o.IDObrok, o.Naziv, o.PostotakBjelancevina, o.PostotakMasti, o.PostotakUgljikohidrata, ok.PostotakHranjiveVrijednost, ok.IDObrokUKombinaciji from Obrok as o
inner join ObrokUKombinaciji as ok
on ok.ObrokID = o.IDObrok
where ok.DnevniSetObrokaID = @IDkombinacije
end
go
create type Obrok as Table
(
ID int,
   IDKombo int,
Naziv nvarchar(50),
Prot int,
Mast int,
Uglj int,
Total int
)
go
select * from Obrok
go
create proc P_UpdateObrok
@XML xml
as
	declare @tablObrok Obrok
	insert into @tablObrok(ID,Naziv, Prot, Mast, Uglj)
      select 
	  tblBolesti.stupacBolest.value('@IDObrok','int') as ID,
	  tblBolesti.stupacBolest.value('@Naziv','nvarchar(MAX)') as Naziv,
	   tblBolesti.stupacBolest.value('@PostotakBjelancevina','int') as Prot,
	   tblBolesti.stupacBolest.value('@PostotakMasti','int') as Mast,
	   tblBolesti.stupacBolest.value('@PostotakUgljikohidrata','int') as Uglj
   from @XML.nodes('/Obrok') as tblBolesti(stupacBolest) 
   select * from @tablObrok
   if(select ID from @tablObrok) > 0
   begin
   update Obrok
   set Naziv = (select Naziv from @tablObrok), PostotakBjelancevina = (select Prot from @tablObrok), PostotakMasti = (select Mast from @tablObrok), PostotakUgljikohidrata = (select Uglj from @tablObrok)
   where IDObrok = (Select ID from @tablObrok)
   end
   else
   begin
   insert into Obrok(Naziv,PostotakBjelancevina,PostotakUgljikohidrata,PostotakMasti,Deleted)
   values((select Naziv from @tablObrok),(select Prot from @tablObrok),(select Uglj from @tablObrok),(select Mast from @tablObrok),0)
   end
   go
   create proc P_Kombinacije
   @ID int
   as
   if @ID = 0
   begin
   select * from DnevniSetObroka
   where Deleted = 0
   end
   else if @ID < 0
   begin
   select * from DnevniSetObroka
   where BrojObrokaUDanu = (@ID * -1)
   end
   else
   begin
	select * from DnevniSetObroka
   where Deleted = 0 and IDDnevniSetObroka = @ID
   end
   go
   create type Kombinacija as table
   (
   ID int,
   Naziv nvarchar(100),
   BrojObroka int,
   Selected nvarchar(100)
   )
   go
   create proc P_UpdateKombinacija
   @XML xml
   as
  
      	declare @tablKombo Kombinacija
	insert into @tablKombo(ID,Naziv, BrojObroka, Selected)
      select 
	  tblBolesti.stupacBolest.value('@IDDnevniSetObroka','int') as ID,
	  tblBolesti.stupacBolest.value('@Naziv','nvarchar(MAX)') as Naziv,
	   tblBolesti.stupacBolest.value('@BrojObrokaUDanu','int') as BrojObroka,
	   tblBolesti.stupacBolest.value('@Selected','nvarchar(MAX)') as Selected
   from @XML.nodes('/Kombinacija') as tblBolesti(stupacBolest) 

   declare @BIT bit
   if(select Selected from @tablKombo) = 'true'
   begin
   set @BIT = 1
   end
   else
   begin
   set @BIT = 0
   end

   declare @SetObroka int

   if (Select ID from @tablKombo) = 0
   begin
   insert into DnevniSetObroka(Naziv,BrojObrokaUDanu,Deleted,Selected)
   values((select Naziv from @tablKombo),(select BrojObroka from @tablKombo),0,  @BIT)
   set @SetObroka = SCOPE_IDENTITY()
   end
   else
   begin
   update DnevniSetObroka
   set Naziv = (select Naziv from @tablKombo), BrojObrokaUDanu = (select BrojObroka from @tablKombo), Selected = @BIT
   where IDDnevniSetObroka = (Select ID from @tablKombo)
   set @SetObroka = (Select ID from @tablKombo)
   end

  DECLARE @cnt INT = 1
  
WHILE @cnt < (select count(*) from @XML.nodes('/Kombinacija/listObroci') as tblGrad(stupacGrad)) + 1
BEGIN
   	declare @tablObrok Obrok
	delete @tablObrok
	insert into @tablObrok(ID,Naziv, Total, IDKombo)
      select 
	  tblBolesti.stupacBolest.value('@IDObrok','int') as ID,
	  tblBolesti.stupacBolest.value('@Naziv','nvarchar(MAX)') as Naziv,
	   tblBolesti.stupacBolest.value('@PostotakHranjiveVrijednost','int') as Total,
	   	   tblBolesti.stupacBolest.value('@IDObrokUKombinaciji','int') as IDKombo
   from @XML.nodes('/Kombinacija/listObroci[sql:variable("@cnt")]') as tblBolesti(stupacBolest) 
	select ID from @tablObrok
   if(select count(*) from ObrokUKombinaciji where ObrokID = (select ID from @tablObrok) and DnevniSetObrokaID = @SetObroka) > 0
   begin
   declare @IDObrokKombinacija int = (Select IDObrokUKombinaciji from ObrokUKombinaciji where ObrokID = (select ID from @tablObrok) and DnevniSetObrokaID = @SetObroka)
   update ObrokUKombinaciji
   set ObrokID = (select ID from @tablObrok), PostotakHranjiveVrijednost = (select Total from @tablObrok)
   where IDObrokUKombinaciji = (select IDKombo from @tablObrok)
   end
   else
   begin
   insert into ObrokUKombinaciji(DnevniSetObrokaID,ObrokID,PostotakHranjiveVrijednost)
   values(@SetObroka,(select ID from @tablObrok),(select Total from @tablObrok))
   end
  set @cnt = @cnt + 1
   end
   
   go
create proc P_Korisnici
@username nvarchar(200),
@password nvarchar(200)
as
if @username = null OR @username=''
begin
select o.Ime, o.Prezime, o.Email, o.KorisnickoIme, k.DatumRodenja, k.Tezina, k.Visina, s.Naziv as Spol, ra.Naziv as RazinaFizickeAktivnosti, td.Naziv as Tip from Korisnik as k
inner join Osoba as o
on o.IDOsoba = k.OsobaID
inner join Spol as s
on s.IDSpol = k.SpolID
inner join TipDijabetesa as td
on td.IDTipDijabetesa = k.TipDijabetesaID
inner join RazinaAktivnosti as ra
on ra.IDRazinaAktivnosti = k.RazinaFizickeAktivnosti
end
else
begin
select k.IDKorisnik,o.Ime, o.Prezime, o.Email, o.KorisnickoIme, k.DatumRodenja, k.Tezina, k.Visina, s.Naziv as Spol, ra.Naziv as RazinaFizickeAktivnosti, td.Naziv as Tip from Korisnik as k
inner join Osoba as o
on o.IDOsoba = k.OsobaID
inner join Spol as s
on s.IDSpol = k.SpolID
inner join TipDijabetesa as td
on td.IDTipDijabetesa = k.TipDijabetesaID
inner join RazinaAktivnosti as ra
on ra.IDRazinaAktivnosti = k.RazinaFizickeAktivnosti
where o.isAdmin = 0 and o.KorisnickoIme = @username and o.Lozinka = @password
end
go
create proc P_GetJelovnik
@date datetime,
@ID int
as
select IDJelovnik, DatumStvaranja, HranjivaVrijednostPotrebna,KorisnikID from Jelovnik
where KorisnikID = @ID and DatumStvaranja = CAST(@date As date)
go
create proc P_GetStavke
@ID int
as
select sj.IDStavkaJelovnika,o.IDObrok, o.Naziv, o.PostotakBjelancevina, o.PostotakMasti, o.PostotakUgljikohidrata, ok.PostotakHranjiveVrijednost, ok.IDObrokUKombinaciji from Obrok as o
inner join ObrokUKombinaciji as ok
on ok.ObrokID = o.IDObrok
inner join StavkaJelovnika as sj
on sj.ObrokUKombinacijiID = ok.IDObrokUKombinaciji
where sj.JelovnikID = @ID
go

select n.IDNamjernica,n.Naziv, mj.Naziv from MjernaJedinicaZaNamjernicu as mzn
inner join MjernaJedinica as mj 
on mj.IDMjernaJedinica = mzn.MjernaJedinicaID
inner join Namjernica as n
on n.IDNamjernica = mzn.NamjernicaID
go
select * from Jelovnik
go
select o.Naziv, n.Naziv,tn.Naziv  from StavkaStavkeJelovnika as ssj
inner join Namjernica as n
on n.IDNamjernica = ssj.NamjernicaID
inner join StavkaJelovnika as sj
on sj.IDStavkaJelovnika = ssj.StavkaJelovnikaID
inner join ObrokUKombinaciji as ouk
on ouk.IDObrokUKombinaciji = sj.ObrokUKombinacijiID
inner join Obrok as o
on o.IDObrok = ouk.ObrokID
inner join TipNamjernice as tn
on tn.IDTipNamjernice = n.TipNamjerniceID
where sj.IDStavkaJelovnika = 7
go
create type Jelovnik as table
(
IDJelovnik int,
Datum date,
TotalHranjiva int,
KorisnikID int
)
go
create type StavkaJelovnika as table
(
IDObrok int,
IDObrokUKombinaciji int,
IDStavkaJelovnika int
)
go
create type Stavka as table
(
IDNamirnica int,
IDStavka int
)
go
create proc P_UpdateJelovnik
@XML xml
as
      	declare @tblJelo Jelovnik
	insert into @tblJelo(IDJelovnik,Datum, TotalHranjiva,KorisnikID)
      select 
	  tblBolesti.stupacBolest.value('@IDJelovnik','int') as IDJelovnik,
	  tblBolesti.stupacBolest.value('@DatumStvaranja','date') as Datum,
	   tblBolesti.stupacBolest.value('@HranjivaVrijednostPotrebna','int') as TotalHranjiva,
	   tblBolesti.stupacBolest.value('@KorisnikID','int') as KorisnikID
   from @XML.nodes('/Jelovnik') as tblBolesti(stupacBolest) 

   if(select IDJelovnik from @tblJelo) = 0
   begin
   insert into Jelovnik(HranjivaVrijednostPotrebna,DatumStvaranja,KorisnikID)
   values((select TotalHranjiva from @tblJelo),(select Datum from @tblJelo), (select KorisnikID from @tblJelo))
   update @tblJelo
   set IDJelovnik = SCOPE_IDENTITY()
   /*Svakako da ovo update cijelu tablicu tbljelo ali prije toga ce vec postojati problemi ako tablica ima vise od jednog reda*/
   end


  DECLARE @cnt INT = 1
  DECLARE @cnt2 INT = 1
WHILE @cnt < (select count(*) from @XML.nodes('/Jelovnik/StavkeJelovnikaList') as tblGrad(stupacGrad)) + 1
BEGIN
   	declare @tablObrok StavkaJelovnika
	delete @tablObrok
	insert into @tablObrok(IDObrok,IDObrokUKombinaciji, IDStavkaJelovnika)
      select 
	  tblBolesti.stupacBolest.value('@IDObrok','int') as IDObrok,
	  tblBolesti.stupacBolest.value('@IDObrokUKombinaciji','int') as IDObrokUKombinaciji,
	   tblBolesti.stupacBolest.value('@IDStavkaJelovnika','int') as IDStavkaJelovnika
   from @XML.nodes('/Jelovnik/StavkeJelovnikaList[sql:variable("@cnt")]') as tblBolesti(stupacBolest) 
   select * from @tablObrok
   	declare @new bit
	set @new = 0
   if(select IDStavkaJelovnika from @tablObrok) = 0
   begin
   insert into StavkaJelovnika(JelovnikID,ObrokUKombinacijiID)
   values((select IDJelovnik from @tblJelo),(select IDObrokUKombinaciji from @tablObrok))
   update @tablObrok
   set IDStavkaJelovnika = SCOPE_IDENTITY()
   set @new = 1
   end
   
   set @cnt2 = 1
 select count(*) from @XML.nodes('/Jelovnik/StavkeJelovnikaList[sql:variable("@cnt")]/NamirniceList') as tblGrad(stupacGrad)  
WHILE @cnt2 < (select count(*) from @XML.nodes('/Jelovnik/StavkeJelovnikaList[sql:variable("@cnt")]/NamirniceList') as tblGrad(stupacGrad)) + 1
BEGIN
   	declare @tablNamirnica Stavka
	delete @tablNamirnica
	insert into @tablNamirnica(IDNamirnica,IDStavka)
      select 
	  tblBolesti.stupacBolest.value('@IDNamjernica','int') as IDNamirnica,
	  tblBolesti.stupacBolest.value('@IDStavkaStavkeJelovnika','int') as IDStavka
   from @XML.nodes('/Jelovnik/StavkeJelovnikaList[sql:variable("@cnt")]/NamirniceList[sql:variable("@cnt2")]') as tblBolesti(stupacBolest) 
   	
   if @new = 1
   begin
   insert into StavkaStavkeJelovnika(StavkaJelovnikaID,NamjernicaID)
   values((select IDStavkaJelovnika from @tablObrok),(select IDNamirnica from @tablNamirnica))
   end
   else
   begin
   update StavkaStavkeJelovnika
   set NamjernicaID = (select IDNamirnica from @tablNamirnica)
   where IDStavkaStavkeJelovnika = (select IDStavka from @tablNamirnica)
   end
   set @cnt2 = @cnt2 + 1
  end 
  set @cnt = @cnt + 1
  end
  
  go
create proc P_Spol
as
select * from Spol
go
create proc P_Aktivnost
as
select * from RazinaAktivnosti
go
create proc P_TipDijabetsa
as
select * from TipDijabetesa
go
create type korisnik as table
(
ID int,
Pwd nvarchar(200),
Ime nvarchar(100),
Prezime nvarchar(100),
Username nvarchar(100),
Email nvarchar(100),
Spol nvarchar(100),
Razina nvarchar(100),
Tip nvarchar(100),
Datum date,
Visina float,
Tezine float
)
go
create proc P_UpdateKorisnik
@XML xml
as

      	declare @tblKorisnik korisnik
	insert into @tblKorisnik(ID,Datum,Ime,Prezime,PWD,Username, Spol, Razina, Tip,  Visina, Tezine, Email)
      select 
	  tblBolesti.stupacBolest.value('@IDKorisnik','int') as ID,
	  tblBolesti.stupacBolest.value('@DatumRodenja','date') as Datum,
	   tblBolesti.stupacBolest.value('@Ime','nvarchar(100)') as Ime,
	   tblBolesti.stupacBolest.value('@Prezime','nvarchar(100)') as Prezime,
	   tblBolesti.stupacBolest.value('@Pwd','nvarchar(200)') as Pwd,
	   tblBolesti.stupacBolest.value('@KorisnickoIme','nvarchar(100)') as Username,
	   tblBolesti.stupacBolest.value('@Spol','nvarchar(100)') as Spol,
	   tblBolesti.stupacBolest.value('@RazinaFizickeAktivnosti','nvarchar(100)') as Razina,
	   tblBolesti.stupacBolest.value('@Tip','nvarchar(100)') as Tip,
	  tblBolesti.stupacBolest.value('@Visina','float') as Visina,
	  tblBolesti.stupacBolest.value('@Tezina','float') as Tezine,
	   tblBolesti.stupacBolest.value('@Email','nvarchar(100)') as Email

   from @XML.nodes('/Korisnik') as tblBolesti(stupacBolest)  
     declare @OsobaID int
   if(select ID from @tblKorisnik) = 0
   begin
   insert into Osoba(Ime,Prezime,KorisnickoIme,Email,Lozinka,isAdmin)
   values((select Ime from @tblKorisnik),(select Prezime from @tblKorisnik),(select Username from @tblKorisnik),(select Email from @tblKorisnik),(select Pwd from @tblKorisnik),0)

   set @OsobaID = SCOPE_IDENTITY()
   insert into Korisnik(OsobaID,DatumRodenja,RazinaFizickeAktivnosti,SpolID,TipDijabetesaID,Tezina,Visina)
   values(@OsobaID, (select Datum from @tblKorisnik),(select IDRazinaAktivnosti from RazinaAktivnosti where Naziv = (select Razina from @tblKorisnik)),(select IDSpol from Spol where Naziv = (select Spol from @tblKorisnik)),(select IDTipDijabetesa from TipDijabetesa where Naziv = (select Tip from @tblKorisnik)),(select Tezine from @tblKorisnik),(select Visina from @tblKorisnik))
   end
   else
   begin
   set @OsobaID = (select OsobaID from Korisnik where IDKorisnik = (select ID from @tblKorisnik))
   update Osoba
   set Ime = (select Ime from @tblKorisnik), Prezime = (select Prezime from @tblKorisnik), KorisnickoIme = (select Username from @tblKorisnik), Email = (select Email from @tblKorisnik)
   where IDOsoba = @OsobaID
   if(select Pwd from @tblKorisnik) !=''
   begin
   update Osoba
   set Lozinka = (select Pwd from @tblKorisnik)
   where IDOsoba = @OsobaID
   end
   update Korisnik
   set DatumRodenja = (select Datum from @tblKorisnik), RazinaFizickeAktivnosti = (select IDRazinaAktivnosti from RazinaAktivnosti where Naziv = (select Razina from @tblKorisnik)), SpolID = (select IDSpol from Spol where Naziv = (select Spol from @tblKorisnik)), TipDijabetesaID = (select IDTipDijabetesa from TipDijabetesa where Naziv = (select Tip from @tblKorisnik)), Tezina = (select Tezine from @tblKorisnik), Visina = (select Visina from @tblKorisnik)
   where IDKorisnik = (select ID from @tblKorisnik)
   end
   go
